let cssLight = "./css/style_light.css";
let cssDark = "./css/style_dark.css";
let style = document.getElementById("theme-style");

function toggle() {  
  if (style.href.match(cssLight)) {
    style.href = cssDark;
    localStorage.setItem("theme", cssDark);
  } else {
    style.href = cssLight;
    localStorage.setItem("theme", cssLight);
  }
}

let changeBtn = document.getElementById("btnTheme");
changeBtn.addEventListener("click", toggle)

window.addEventListener("load", function () {
    let theme = localStorage.getItem("theme");
    if (theme) {
      style.setAttribute("href", `${theme}`);
    }
});